//
//  NetworkHelper.swift
//  Assingment
//
//  Created by Aman Gupta on 12/07/19.
//  Copyright © 2019 Aman Gupta. All rights reserved.
//

import Foundation
import UIKit

protocol NetworkHelperDelegate {
    func parseJson(jsonData:[String:Any])
    func generateErrorMessage(error type:String,message:String)
    func addToImages(image:UIImage,for movie:String)
}
extension NetworkHelperDelegate {
    func addToImages(image:UIImage,for movie:String) {}
}
class NetworkHelper {

    var networkDelegate : NetworkHelperDelegate?
    
    func fetchData(url : URL, networkDelegate: NetworkHelperDelegate?) {
        self.networkDelegate = networkDelegate
        let session = URLSession(configuration: .ephemeral)
        let task = session.dataTask(with: url) { (data, response, error) in
            if error == nil , let dataRecieved = data{
                if let jsonData = try? JSONSerialization.jsonObject(with: dataRecieved, options: []) as? [String:Any]{
                    self.networkDelegate?.parseJson(jsonData: jsonData)
                }
                else{
                    networkDelegate?.generateErrorMessage(error: "Network Error", message: "Please Check Network Setting")
                }
            }
            else {
                networkDelegate?.generateErrorMessage(error: "Network Error", message: "Please Check Network Setting")
            }
            
        }
        task.resume()
    }
    func fetchImage(for movie:String,from url:String,networkDelegate:NetworkHelperDelegate){
        if let defaultImage = UIImage(named: "img"){
            if(url.elementsEqual("N/A") == true){
                self.networkDelegate?.addToImages(image: defaultImage, for: movie)
            }
            else{
                if let url1 = URL(string: url){
                    let downloadedPic = URLSession.shared.dataTask(with: url1) { (data, response, error) in
                        if let _ = error{
                            self.networkDelegate?.addToImages(image: defaultImage, for: movie)
                        }
                        else{
                            if let image = data{
                                
                                if let imageParsed = UIImage(data: image){
                                    
                                    self.networkDelegate?.addToImages(image: imageParsed,for:movie)
                                }
                                else{
                                    self.networkDelegate?.addToImages(image: defaultImage, for: movie)
                                }
                            }
                        }
                    }
                    downloadedPic.resume()
                }
            }
        }
    }
    
}
