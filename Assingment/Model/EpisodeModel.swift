//
//  EpisodeModel.swift
//  Assingment
//
//  Created by Aman Gupta on 18/07/19.
//  Copyright © 2019 Aman Gupta. All rights reserved.
//

import Foundation

class EpisodeModel{
    var episodeTitle : String = ""
    var episodeNumber : String = ""
    var imdbRating : String = ""
    var imdbID : String = ""
}
