//
//  DataModel.swift
//  Assingment
//
//  Created by Aman Gupta on 11/07/19.
//  Copyright © 2019 Aman Gupta. All rights reserved.
//

import Foundation
import UIKit
class DataModel {
    var title:String = "N/A"
    var year:String = "N/A"
    var imdbRating:String = "N/A"
    var imdbId:String = ""
    var actors:String = "N/A"
    var genre:String = "N/A"
    var numberOfSeasons:String?
}
