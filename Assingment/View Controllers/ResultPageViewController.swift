//
//  ResultPageViewController.swift
//  Assingment
//
//  Created by Aman Gupta on 22/07/19.
//  Copyright © 2019 Aman Gupta. All rights reserved.
//

import UIKit

class ResultPageViewController: UIPageViewController,UIPageViewControllerDelegate {
    
    var searchedQuery : String?
    var segmentControl = UISegmentedControl()
    
    private lazy var moviesView : MovieViewController? = {
        let storyBoard = UIStoryboard(name: "Main", bundle: Bundle.main)
        if let controllerObj = storyBoard.instantiateViewController(withIdentifier: "movieStoryboard") as? MovieViewController{
            return controllerObj
        }
        else {
            return nil
        }
    }()
    
    private lazy var seriesView : SeriesViewController? = {
        let storyBoard = UIStoryboard(name: "Main", bundle: Bundle.main)
        if let controllerObj = storyBoard.instantiateViewController(withIdentifier: "seriesStoryboard") as? SeriesViewController{
            
            return controllerObj
        }
        else {
            return nil
        }
    }()
    
    private lazy var viewControllerArray:[UIViewController]={
        var viewControllerArray = [UIViewController]()
        if let moviesController = self.moviesView{
            viewControllerArray.append(moviesController)
        }
        if let seriesController = self.seriesView{
            viewControllerArray.append(seriesController)
        }
        return viewControllerArray
    }()
    func setViewOnPageViewFor(index:Int){
        let viewController = viewControllerArray[index]
        setViewControllers([viewController], direction: .forward, animated: true, completion: nil)
        if index == 0{
            let vc = viewControllers?.first as? MovieViewController
            vc?.searchedQuery = self.searchedQuery
        }
        else {
            let vc = viewControllers?.first as? SeriesViewController
            vc?.searchedQuery = self.searchedQuery
        }
        
        
    }
    func setupSegments()
    {
        segmentControl.removeAllSegments()
        segmentControl.insertSegment(withTitle: "Movies", at: 0, animated: true)
        segmentControl.insertSegment(withTitle: "Series", at: 1, animated: true)
        segmentControl.addTarget(self, action: #selector(self.selectionChanged), for: .valueChanged)
        
        segmentControl.selectedSegmentIndex=0
        
    }
    @objc func selectionChanged(){
        
        if segmentControl.selectedSegmentIndex == 0{
            setViewOnPageViewFor(index: 0)
        }
        else if segmentControl.selectedSegmentIndex == 1{
            setViewOnPageViewFor(index: 1)
            
        }
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
//        let window = UIApplication.shared.windows[0]
        applyGradient()
        
        segmentControl.setTitleTextAttributes([NSAttributedString.Key.foregroundColor:UIColor.purple], for: [])
        segmentControl.tintColor = UIColor.purple
        segmentControl.backgroundColor = UIColor.white
        segmentControl.translatesAutoresizingMaskIntoConstraints = false

        view.addSubview(segmentControl)
        segmentControl.bringSubviewToFront(view)
        segmentControl.topAnchor.constraint(equalToSystemSpacingBelow: self.view.safeAreaLayoutGuide.topAnchor, multiplier: 0).isActive = true
        segmentControl.centerXAnchor.constraint(equalTo: self.view.centerXAnchor).isActive = true
        segmentControl.heightAnchor.constraint(equalToConstant: 27).isActive = true
        segmentControl.widthAnchor.constraint(equalToConstant: UIScreen.main.bounds.width).isActive = true
        

        
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = UIColor.white
        
        self.delegate = self
        self.dataSource = self
        setupSegments()
        setViewOnPageViewFor(index: 0)
        
    }

}
extension ResultPageViewController:UIPageViewControllerDataSource{
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        guard let currentViewIndex = viewControllerArray.firstIndex(of: viewController) else {
            return nil
        }
        let previousIndex = currentViewIndex-1
        guard previousIndex>=0 else {
            return nil
        }
        guard viewControllerArray.count > previousIndex else{
            return nil
        }
        
        return viewControllerArray[previousIndex]
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        guard let currentViewIndex = viewControllerArray.firstIndex(of: viewController) else {
            return nil
        }
        let nextIndex = currentViewIndex+1
        guard nextIndex<viewControllerArray.count else {
            return nil
        }
        
        return viewControllerArray[nextIndex]
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool) {
        if completed , let firstViewContollerInPageView = pageViewController.viewControllers?.first
        {
            if let newSegmentViewIndex = self.viewControllerArray.firstIndex(of:firstViewContollerInPageView){
                self.segmentControl.selectedSegmentIndex = newSegmentViewIndex
            }
            switch firstViewContollerInPageView {
            case let vc as MovieViewController:
                vc.searchedQuery = self.searchedQuery
            case let vc as SeriesViewController:
                vc.searchedQuery = self.searchedQuery
            default:
                break
                
            }
        }
    }
}
//MARK:gradient and orientation
extension ResultPageViewController{
    
    func applyGradient()
    {
        let gradientLayer = CAGradientLayer()
        
        gradientLayer.frame = self.view.bounds
        
        gradientLayer.colors = [UIColor.purple.cgColor,#colorLiteral(red: 0.5490759835, green: 0, blue: 0.2728373455, alpha: 1).cgColor]
        
        self.view.layer.insertSublayer(gradientLayer, at: 0)
    }
    
    
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)
        coordinator.animate(alongsideTransition: nil) { _ in
            self.view.layer.sublayers?.remove(at: 0)
            self.applyGradient()
        }
    }
    
    
}
