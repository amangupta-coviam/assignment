//
//  ViewController.swift
//  Assingment
//
//  Created by Aman Gupta on 11/07/19.
//  Copyright © 2019 Aman Gupta. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    
    //MARK:gradient and orientation
    func applyGradient()
    {
        
        let gradientLayer = CAGradientLayer()
        gradientLayer.frame = self.view.bounds
        gradientLayer.colors = [UIColor.purple.cgColor,UIColor.red.cgColor]
        gradientLayer.startPoint = CGPoint(x: 0, y: 0)
        gradientLayer.endPoint = CGPoint(x: 0, y: 1)
        self.view.layer.insertSublayer(gradientLayer, at: 0)
    }
    //Gradient
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        applyGradient()
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)
        coordinator.animate(alongsideTransition: nil) { _ in
            self.view.layer.sublayers?.remove(at: 0)
            self.applyGradient()
        }
        
    }

    @IBOutlet weak var buttonObject: UIButton!
    @IBOutlet weak var searchField: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        searchField.layer.opacity = 0.5
        searchField.backgroundColor = #colorLiteral(red: 0.2023952174, green: 0.04690541338, blue: 0.4568131345, alpha: 1)
        searchField.textColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        searchField.tintColor = UIColor.white
        searchField.attributedPlaceholder = NSAttributedString(string: NSLocalizedString("Search", comment: ""),attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])
        buttonObject.layer.opacity = 0.8
        
        buttonObject.layer.cornerRadius = 4.0
        

        
    }

    @IBAction func searchButtonPressed(_ sender: UIButton) {
        if(searchField.text == ""){
            searchField.becomeFirstResponder()
        }
        else{
            performSegue(withIdentifier: "moveToResult", sender: self)
            
        }
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let destinationVC = segue.destination as? ResultPageViewController , let searchText = searchField.text{
            destinationVC.searchedQuery = searchText.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
        }
    }
    
}


extension UIView {
    func showProgressIndicator(){
        
        let view = UIView()
        view.frame = CGRect(x: 0, y: 0, width: 80, height: 80)
        view.center = self.center
        view.backgroundColor = #colorLiteral(red: 0.2666666667, green: 0.2666666667, blue: 0.2666666667, alpha: 0.7)
        view.clipsToBounds = true
        view.tag = 110
        view.layer.cornerRadius = 10
        
        let progressIndicator = UIActivityIndicatorView()
        progressIndicator.frame = CGRect(x: 0, y: 0, width: 30, height: 40)
        progressIndicator.center = CGPoint(x: view.frame.width/2, y: view.frame.height/2)
        progressIndicator.style = .whiteLarge
        
        view.addSubview(progressIndicator)
        self.addSubview(view)
        progressIndicator.startAnimating()
    }
    func stopAnimation()
    {
        if let progress = self.viewWithTag(110){
            progress.removeFromSuperview()
        }
    }
}
