//
//  EpisodesTableViewController.swift
//  Assingment
//
//  Created by Aman Gupta on 17/07/19.
//  Copyright © 2019 Aman Gupta. All rights reserved.
//

import UIKit

class EpisodesTableViewController: UIViewController {
    
    
    @IBOutlet var tableView: UITableView!
    var networkHelper = NetworkHelper()
    var imdbId : String?{
        didSet{
            let url = URL(string: "http://www.omdbapi.com/?apikey=f749e596&i=\(imdbId!)")
            self.networkHelper.fetchData(url: url!, networkDelegate: self)
        }
    }
    var setNumberOfSeasonsOnlyOnce:Bool = false
    var seasonEpisodes : [Int:EpisodeModel?] = [:]
    var season = [Int:[EpisodeModel]]()
    var numberOfSeason:Int=0{
        didSet{
            for i in 1...numberOfSeason{
                season[i] = []
//                tableView.reloadData()
                print("Season number \(i)")
                fetchEpisodeDetailsFor(season: i)
            }
            
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
//        self.tableView.separatorStyle = .singleLine
//        self.tableView.separatorColor = UIColor.blue
        
        
    }
    
    func fetchEpisodeDetailsFor(season:Int)
    {
        let url = URL(string: "http://www.omdbapi.com/?apikey=f749e596&i=\(imdbId!)&season=\(season)")
        self.networkHelper.fetchData(url: url!, networkDelegate: self)
    }
    
    //MARK:Table View Functions
    
  

    
    
   
//}
//extension EpisodesTableViewController : UICollectionViewDelegate,UICollectionViewDataSource{
//    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
//        return season[section]?.count ?? 10;
//    }
//
//    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
//        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "episodeCell", for: indexPath)
//        if let titleLabel = cell.viewWithTag(101) as? UILabel{
//            titleLabel.text = imdbId!
//        }
//        if let episodeLabel = cell.viewWithTag(102) as? UILabel{
//            episodeLabel.text = "\(indexPath.row)"
//
//        }
//
//
//
//        return cell
//    }
//
//}
//extension EpisodesTableViewController : UITableViewDataSource,UITableViewDelegate{
//     func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//        // #warning Incomplete implementation, return the number of rows
//        return 1
//    }
//
//     func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
//        let cell = tableView.dequeueReusableCell(withIdentifier: "tableCell", for: indexPath)
//        //        if let episode = seasonEpisodes[indexPath.row+1]{
//        //            print(episode)
//        //        }
//        //        else {
//        //            fetchEpisodeDetailsFor(season: indexPath.row+1)
//        //        }
//        print("row added \(numberOfSeason) \(indexPath.row)")
//        return cell
//    }
//
//    func numberOfSections(in tableView: UITableView) -> Int {
//        return numberOfSeason
//    }
//
//     func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
//        return 10.0
//    }
//     func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
//        let view = UIView()
//        view.backgroundColor = UIColor.blue
//        let label = UILabel(frame: CGRect(x: 0, y: 0, width: self.tableView.frame.width, height: 40))
//        label.text = "Season \(section+1)"
//        label.textColor = UIColor.black
//        view.addSubview(label)
//        return view
//    }
//
//}
}
extension ViewController : UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 10
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = Bundle.main.loadNibNamed("TestTableViewCell", owner: self, options: nil)?.first as! TestTableViewCell
        
        return cell
    }
    
    
}
extension EpisodesTableViewController:NetworkHelperDelegate{
    func parseJson(jsonData: [String : Any]) {
        if (jsonData["Response"] as! String) == "False"{
//            let alert = UIAlertController(title: "Some Error Occured", message: "Try Again", preferredStyle:.alert)
//            let action = UIAlertAction(title: "Go Back", style: .default) { (action) in
//                self.navigationController?.popViewController(animated: true)
//            }
//            alert.addAction(action)
//
//            self.present(alert,animated: true,completion: nil)
        }
        else {
            if let totalSeasons = jsonData["totalSeasons"]! as? String , !setNumberOfSeasonsOnlyOnce{
                print("setting number of seasons")
                numberOfSeason = Int(totalSeasons)!
                DispatchQueue.main.async {

                }
                setNumberOfSeasonsOnlyOnce = true
            }
            if let currentSeasonEpisodes = jsonData["Episodes"] as? [[String:String]] {
                print("Going to fetch data")
                let currentSeasonNumber = Int((jsonData["Season"]! as? String)!)
                var seasonDetail = season[currentSeasonNumber!]
//                seasonDetail![0].episodeTitle = currentSeasonEpisodes[0]["Title"]!
                for currentEpisode in currentSeasonEpisodes{
                    let episode = EpisodeModel()
                    episode.episodeNumber = currentEpisode["Episode"]!
                    episode.episodeTitle = currentEpisode["Title"]!
                    seasonDetail?.append(episode)
                }
                
                season[currentSeasonNumber!]=seasonDetail}
            
            
            
            
        }
    }
    func printEpisodesFor(season:Int){
        let episodes = self.season[season]
        print("Episodes in \(season)")
        for episode in episodes!{
            print(episode.episodeTitle)
            print(episode.episodeNumber)
        }
    }
    
    func generateErrorMessage() {
        let alert = UIAlertController(title: "Network Error", message: "Please Check Your Network Settings", preferredStyle: .alert)
        let action = UIAlertAction(title: "Ok", style: .default) { (action) in
            self.navigationController?.popViewController(animated: true)
        }
        alert.addAction(action)
        self.present(alert,animated: true,completion: nil)
    }
    
    func addToImages(image: UIImage, for movie: String) {
        
    }
    
    
}

