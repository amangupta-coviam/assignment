//
//  EpisodeController.swift
//  Assingment
//
//  Created by Aman Gupta on 18/07/19.
//  Copyright © 2019 Aman Gupta. All rights reserved.
//

import UIKit

class EpisodeController: UITableViewController {

    var numberOfSeasonsDetailsRecieved = 0
    var networkHelper = NetworkHelper()
    var recievedImdbId : String?{
        didSet{
            if let id = recievedImdbId , let url = URL(string: "http://www.omdbapi.com/?apikey=f749e596&i=\(id)"){
                self.networkHelper.fetchData(url: url, networkDelegate: self)
            }
        }
    }
    
    var setNumberOfSeasonsOnlyOnce:Bool = false
    var seasonEpisodeDetails = [Int:[EpisodeModel]]()
    var choosenEpisodeImdbId:String = ""
    var numberOfSeasons:Int=0{
        didSet{
            if numberOfSeasons != 0{
                for i in 1...numberOfSeasons{
                    seasonEpisodeDetails[i] = []
                    fetchEpisodeDetailsFor(season: i)
                }
            }
            else{
                DispatchQueue.main.async {
                    self.view.stopAnimation()
                }
                generateErrorMessage(error: "No Data Available", message: "No data is available for this series")
            }
        }
    }
    func fetchEpisodeDetailsFor(season:Int)
    {
        if let id = recievedImdbId , let url = URL(string: "http://www.omdbapi.com/?apikey=f749e596&i=\(id)&season=\(season)"){
                self.networkHelper.fetchData(url: url, networkDelegate: self)
        }
    }

    fileprivate func applyGradient() {
        UIGraphicsBeginImageContext(self.view.frame.size)
        UIImage(named: "gradient-image")?.draw(in: self.view.bounds)
        if let image = UIGraphicsGetImageFromCurrentImageContext(){
            UIGraphicsEndImageContext()
            self.view.backgroundColor = UIColor(patternImage: image)
            self.view.layer.contents = image.cgImage
        }
        else {
            UIGraphicsEndImageContext()
            self.view.backgroundColor = UIColor.blue
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        applyGradient()
        
        tableView.separatorStyle  = .none
        self.view.showProgressIndicator()
    }

    // MARK: - Table view data source

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return numberOfSeasons
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = Bundle.main.loadNibNamed("TestTableViewCell", owner: self, options: nil)?.first as? TestTableViewCell else {return UITableViewCell()}
        cell.backgroundColor = UIColor.clear.withAlphaComponent(0)
        cell.tableViewCellDelegate = self
        cell.currentSeason = indexPath.row+1
        cell.backgroundColor = UIColor.clear.withAlphaComponent(0)
        if let episode = seasonEpisodeDetails[indexPath.row+1]
        {
            if(episode.count>0)
            {
                var episodesList:[String]=[]
                var imdbIds:[String]=[]
                for e in episode{
                    episodesList.append("\(e.episodeTitle)\nS\(indexPath.row+1) E\(e.episodeNumber)\nRating:\(e.imdbRating)")
                    imdbIds.append(e.imdbID)
                }
                cell.imdbIds = imdbIds
                cell.title = episodesList
            }
        }
        
        return cell
    }
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 180
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "detailSegue" , let destinationVC = segue.destination as? DetailsViewController{
            destinationVC.imdbId = choosenEpisodeImdbId
        }
    }
 
}

extension EpisodeController : TableViewCellDelegate{
    func performSegueFor(imdbId:String) {
        choosenEpisodeImdbId = imdbId
        self.performSegue(withIdentifier: "detailSegue", sender: self)
        
    }
    
    
}

//MARK:Network delegate

extension EpisodeController:NetworkHelperDelegate{
    func parseJson(jsonData: [String : Any]) {
        
        if let response = jsonData["Response"] as? String , response == "False"{
            numberOfSeasonsDetailsRecieved += 1
        }
        else {
            if let seasonsNum = jsonData["totalSeasons"] as? String , !setNumberOfSeasonsOnlyOnce{
                numberOfSeasons = Int(seasonsNum) ?? 0
                setNumberOfSeasonsOnlyOnce = true
            }
            
            else if let currentSeasonEpisodes = jsonData["Episodes"] as? [[String:String]] , let seasonNumber = (jsonData["Season"] as? String) , let currentSeasonNumber = Int(seasonNumber){
                numberOfSeasonsDetailsRecieved += 1
                var seasonDetail = seasonEpisodeDetails[currentSeasonNumber]
                for currentEpisode in currentSeasonEpisodes{
                    let episode = EpisodeModel()
                    if let episodeNum = currentEpisode["Episode"]{
                        episode.episodeNumber = episodeNum
                    }
                    if let title = currentEpisode["Title"]{
                        episode.episodeTitle = title
                    }
                    if let rating = currentEpisode["imdbRating"]{
                        episode.imdbRating = rating
                    }
                    if let imdbId = currentEpisode["imdbID"]{
                        episode.imdbID = imdbId
                    }
                    seasonDetail?.append(episode)
                }
                seasonEpisodeDetails[currentSeasonNumber]=seasonDetail
                
                if numberOfSeasonsDetailsRecieved == seasonEpisodeDetails.count{
                    DispatchQueue.main.async {
                        self.view.stopAnimation()
                        self.tableView.reloadData()
                    }
                }
            }
            else {
                generateErrorMessage(error: "Error", message: "Information Not Avaialable")
            }
            
        }
        if numberOfSeasonsDetailsRecieved == seasonEpisodeDetails.count{
            DispatchQueue.main.async {
                self.view.stopAnimation()
                self.tableView.reloadData()
            }
        }
        
    }
    func generateErrorMessage(error type:String,message:String) {
        let alert = UIAlertController(title: type, message: message, preferredStyle: .alert)
        let action = UIAlertAction(title: "Ok", style: .default) { (action) in
            self.navigationController?.popViewController(animated: true)
        }
        alert.addAction(action)
        self.present(alert,animated: true,completion: nil)
    }

}
