//
//  DetailsViewController.swift
//  Assingment
//
//  Created by Aman Gupta on 11/07/19.
//  Copyright © 2019 Aman Gupta. All rights reserved.
//

import UIKit

class DetailsViewController: UIViewController {

    
    var data:String=""
    var networkHelper = NetworkHelper()
    
    
    @IBOutlet weak var tableView: UITableView!
    var titleString = ""
    var yearString = ""
    var actorString = ""
    var genreString = ""
    var ratingString = ""
    var plotString = ""
    var posterImage = UIImage(named: "img")
    
    var imdbId : String? {
        didSet{
            if let id = imdbId , let url = URL(string: "http://www.omdbapi.com/?apikey=f749e596&i=\(id)"){
                networkHelper.fetchData(url: url, networkDelegate: self)
            }
            
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        
        UIGraphicsBeginImageContext(self.view.frame.size)
        UIImage(named: "gradient-image")?.draw(in: self.view.bounds)
        if let image = UIGraphicsGetImageFromCurrentImageContext(){
            UIGraphicsEndImageContext()
            self.view.backgroundColor = UIColor(patternImage: image)
            self.view.layer.contents = image.cgImage
        }
        else {
            UIGraphicsEndImageContext()
            self.view.backgroundColor = UIColor.blue
        }
        
        tableView.backgroundColor = UIColor.clear.withAlphaComponent(0)
        tableView.separatorStyle = .none
        tableView.allowsSelection = false
        self.view.showProgressIndicator()
        
    }
    
}

extension DetailsViewController : UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 0{
            return 245
        }
        else {
            return 400
        }
    
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0{
            if let cell = Bundle.main.loadNibNamed("ImageTableViewCell", owner: self, options: nil)?.first as? ImageTableViewCell{
                cell.backgroundColor = UIColor.clear.withAlphaComponent(0)
                cell.imageView1.image = posterImage
                return cell
            }
        }
        else if let cell = Bundle.main.loadNibNamed("DetailsTableViewCell", owner: self, options: nil)?.first as? DetailsTableViewCell{
                cell.backgroundColor = UIColor.clear.withAlphaComponent(0)
                cell.title.text = self.titleString
                cell.actor.text = self.actorString
                cell.year.text = self.yearString
                cell.genre.text = self.genreString
                cell.plot.text = self.plotString
                cell.rating.text = self.ratingString
                return cell
        }
        return UITableViewCell()
    }
    
    
}

extension DetailsViewController:NetworkHelperDelegate{
    func generateErrorMessage(error type:String,message:String) {
        let alert = UIAlertController(title: type, message: message, preferredStyle: .alert)
        let action = UIAlertAction(title: "Ok", style: .default) { (action) in
            self.navigationController?.popViewController(animated: true)
        }
        alert.addAction(action)
        self.present(alert,animated: true,completion: nil)
    }
    
    func parseJson(jsonData: [String : Any]) {
        if let response = jsonData["Response"] as? String , response == "False"{
            let alert = UIAlertController(title: "Some Error Occured", message: "Try Again", preferredStyle:.alert)
            let action = UIAlertAction(title: "Go Back", style: .default) { (action) in
                self.navigationController?.popViewController(animated: true)
            }
            alert.addAction(action)
            self.present(alert,animated: true,completion: nil)
        }
        else {
            if let title = jsonData["Title"] as? String{
                self.titleString = title
            }
            if let year = jsonData["Year"] as? String{
                self.yearString = year
            }
            if let actors = jsonData["Actors"] as? String{
            self.actorString = "Actors:\(actors)\n"
            }
            if let genre = jsonData["Genre"] as? String{
                self.genreString = "Genre:\(genre)\n"
            }
            if let ratingString = jsonData["imdbRating"] as? String{
                self.ratingString = "Rating:\(ratingString)\n"
            }
            if let plotString = jsonData["Plot"] as? String{
                self.plotString = "Plot:\(plotString)\n"
            }
            
            if let imageUrl = jsonData["Poster"] as? String{
                networkHelper.fetchImage(for: "movie", from: imageUrl, networkDelegate: self)
            }
            else{
                DispatchQueue.main.async {
                    self.view.stopAnimation()
                    self.tableView.reloadData()
                }
            }
        }
        
    }
    func addToImages(image: UIImage,for movie:String) {
        posterImage = image
        DispatchQueue.main.async {
            self.view.stopAnimation()
            self.tableView.reloadData()
        }
    }
    
}

