//
//  SeriesViewController.swift
//  Assingment
//
//  Created by Aman Gupta on 11/07/19.
//  Copyright © 2019 Aman Gupta. All rights reserved.
//

import UIKit

class SeriesViewController: UIViewController {

    var imagesRecievedCounter = 0
    var data : [DataModel] = []
    var imageData:[String:UIImage] = [:]
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var seriesTableView: UITableView!
    var networkHelper = NetworkHelper()
    var choosenSeriesImdbId : String = ""
    
    fileprivate func doNetworkCall() {
        data=[]
        imagesRecievedCounter = 0
        if let query = searchedQuery , let urlString = URL(string: "http://www.omdbapi.com/?apikey=f749e596&type=series&s=\(query)"){
            self.view.showProgressIndicator()
            networkHelper.fetchData(url: urlString, networkDelegate: self)
        }
    }
    
    var searchedQuery:String?{
        didSet{
            doNetworkCall()
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = UIColor.clear.withAlphaComponent(0)
        tableView.backgroundColor = UIColor.clear.withAlphaComponent(0)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "episodesSegue" , let destinationVc = segue.destination as? EpisodeController{
            destinationVc.recievedImdbId = choosenSeriesImdbId
        }
    }
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)
        coordinator.animate(alongsideTransition: nil) { _ in
            self.tableView.reloadData()
        }
    }
}

extension SeriesViewController:UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return data.count
    }
    
    fileprivate func applyCellDesign(on cell: UITableViewCell) {
        DispatchQueue.main.async {
            cell.backgroundColor = UIColor.clear
            cell.layer.cornerRadius = 10
            let shadowPath2 = UIBezierPath(rect: cell.bounds)
            cell.layer.masksToBounds = false
            cell.layer.shadowColor = UIColor.black.cgColor
            cell.layer.shadowOffset = CGSize(width: CGFloat(1.0), height: CGFloat(3.0))
            cell.layer.shadowOpacity = 0.4
            cell.layer.shadowPath = shadowPath2.cgPath
            cell.layoutIfNeeded()
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "seriesCell",for: indexPath)
        
        //Shadows and designing
        //data allocation
        if indexPath.row < data.count{
        
            if let label = cell.viewWithTag(20) as? UILabel {
                label.text=data[indexPath.row].title
                label.textColor = UIColor.white
            }
            if let label = cell.viewWithTag(21) as? UILabel {
                label.text=data[indexPath.row].year
                label.textColor = UIColor.white
            }
        }
        if indexPath.row < data.count{
            if let imageView = cell.viewWithTag(22) as? UIImageView{
                imageView.image = imageData[data[indexPath.row].imdbId]
            }
        }
        applyCellDesign(on:cell)
        cell.selectionStyle = .none
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        choosenSeriesImdbId = data[indexPath.row].imdbId

        performSegue(withIdentifier: "episodesSegue", sender: self)
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    
    
}

extension SeriesViewController:NetworkHelperDelegate{
    func parseJson(jsonData: [String : Any]) {
        if let response = jsonData["Response"] as? String , response == "False"{
            generateErrorMessage(error:"No Such Records Found", message: "Please Refine Search Term")
        }
        else if let s2 = jsonData["Search"] as? [[String:String]]
        {
            for obj in s2 {
                
                let dataModel = DataModel()
                if let title = obj["Title"]{
                    dataModel.title=title
                }
                if let year = obj["Year"]{
                    dataModel.year=year
                }
                if let imdbId = obj["imdbID"]{
                    dataModel.imdbId=imdbId
                }
                if let imageUrl=(obj["Poster"])
                {
                    imageData[dataModel.imdbId]=UIImage(named: "img")
                    networkHelper.fetchImage(for: dataModel.imdbId, from: imageUrl, networkDelegate: self)
                }
                else{
                    imagesRecievedCounter += 1
                    
                    imageData[dataModel.imdbId]=UIImage(named: "img")
                }
                self.data.append(dataModel)
            }
        }
        
        
    }
    func generateErrorMessage(error type:String,message:String) {
        let alert = UIAlertController(title: type, message: message, preferredStyle: .alert)
        let action = UIAlertAction(title: "Ok", style: .default) { (action) in
            self.navigationController?.popViewController(animated: true)
        }
        alert.addAction(action)
        self.present(alert,animated: true,completion: nil)
    }
    
    func addToImages(image: UIImage,for movie:String) {
        imagesRecievedCounter += 1
        imageData[movie] = image
        if imagesRecievedCounter == imageData.count{
            DispatchQueue.main.async {
                self.view.stopAnimation()
                self.tableView.reloadData()
            }
        }
    }
    
    
}

