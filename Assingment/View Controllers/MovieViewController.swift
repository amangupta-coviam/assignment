//
//  MovieViewController.swift
//  Assingment
//
//  Created by Aman Gupta on 11/07/19.
//  Copyright © 2019 Aman Gupta. All rights reserved.
//

import UIKit


class MovieViewController: UIViewController {

    var imagesRecievedCounter = 0
    var data:[DataModel] = []
    var imageData:[String:UIImage] = [:]

    @IBOutlet weak var collectionViewObject: UICollectionView!
    @IBOutlet weak var movieVIewController: UICollectionView!
    
    var networkHelper = NetworkHelper()
    var choosenMovieImdbId : String = ""
    
    fileprivate func doNetworkCall() {
        data=[]
        imagesRecievedCounter = 0

        if let query = searchedQuery , let urlString = URL(string: "http://www.omdbapi.com/?apikey=f749e596&type=movie&s=\(query)") {
            self.view.showProgressIndicator()
            networkHelper.fetchData(url: urlString, networkDelegate: self)
        }
    }
    
    var searchedQuery : String?{
        
        didSet{
            doNetworkCall()
            
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        collectionViewObject.backgroundColor = UIColor.clear.withAlphaComponent(0)
        view.backgroundColor = UIColor.clear.withAlphaComponent(0)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "detailSegue" , let destinationVC = segue.destination as? DetailsViewController{
            destinationVC.imdbId = choosenMovieImdbId
        }
        
    }

}

//MARK:-Collection view functions

extension MovieViewController : UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return data.count
    }
    
    fileprivate func applyShadow(on cell: UICollectionViewCell) {
        //Shadows and designing
        cell.layer.cornerRadius = 10
        let shadowPath2 = UIBezierPath(rect: cell.bounds)
        cell.layer.masksToBounds = false
        cell.layer.shadowColor = UIColor.black.cgColor
        cell.layer.shadowOffset = CGSize(width: CGFloat(1.0), height: CGFloat(3.0))
        cell.layer.shadowOpacity = 0.4
        cell.layer.shadowPath = shadowPath2.cgPath
        cell.backgroundColor = UIColor.clear.withAlphaComponent(0)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell=collectionView.dequeueReusableCell(withReuseIdentifier: "movieCell", for: indexPath)
        
        applyShadow(on:cell)
        //Data allocation
        if indexPath.row < data.count{
            
            if let titleLabel = cell.viewWithTag(10) as? UILabel {
                titleLabel.text = data[indexPath.row].title
                titleLabel.backgroundColor = UIColor.purple.withAlphaComponent(0.5)
                titleLabel.textColor = UIColor.white
            }
            if let yearLabel = cell.viewWithTag(11) as? UILabel {
                yearLabel.text = data[indexPath.row].year
                yearLabel.backgroundColor = UIColor.purple.withAlphaComponent(0.5)
                yearLabel.textColor = UIColor.white
            }
        }
        if indexPath.row < imageData.count{
            if let img = cell.viewWithTag(13) as? UIImageView{
                img.image = imageData[data[indexPath.row].imdbId]
                img.layer.cornerRadius = 10
                img.layer.masksToBounds = true
            }
        }
        return cell
    }
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        choosenMovieImdbId = data[indexPath.row].imdbId
        performSegue(withIdentifier: "detailSegue", sender: self)
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize(width: 110, height: 163)
    }
    
    
    

    
}

extension MovieViewController : NetworkHelperDelegate {
    func parseJson(jsonData:[String:Any]){
        if let response = jsonData["Response"] as? String , response == "False"{
            generateErrorMessage(error: "No Records Found", message: "Please Refine Search Term")
        }
        else if let s2 = jsonData["Search"] as? [[String:String]]
        {
            for obj in s2 {
                let dataModel = DataModel()
                if let title = obj["Title"]{
                    dataModel.title=title
                }
                if let year = obj["Year"]{
                    dataModel.year=year
                }
                if let imdbId = obj["imdbID"]{
                    dataModel.imdbId=imdbId
                }
                if let imageUrl=(obj["Poster"])
                {
                    imageData[dataModel.imdbId]=UIImage(named: "img")
                    networkHelper.fetchImage(for: dataModel.imdbId, from: imageUrl, networkDelegate: self)
                }
                else{
                    imagesRecievedCounter += 1

                    imageData[dataModel.imdbId]=UIImage(named: "img")
                }
                self.data.append(dataModel)
            }
        }
        
    }
    func generateErrorMessage(error type:String,message:String) {
        let alert = UIAlertController(title: type, message: message, preferredStyle: .alert)
        let action = UIAlertAction(title: "Ok", style: .default) { (action) in
            self.navigationController?.popViewController(animated: true)
        }
        alert.addAction(action)
        self.present(alert,animated: true,completion: nil)
    }
    func addToImages(image:UIImage,for movie:String){
        imagesRecievedCounter += 1
        imageData[movie] = image
        if imagesRecievedCounter == imageData.count{
            DispatchQueue.main.async {
                self.view.stopAnimation()
                self.collectionViewObject.reloadData()
            }
        }
        
    }
    
    
    
    
    
}
