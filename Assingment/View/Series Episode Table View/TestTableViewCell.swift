//
//  TestTableViewCell.swift
//  Assingment
//
//  Created by Aman Gupta on 18/07/19.
//  Copyright © 2019 Aman Gupta. All rights reserved.
//

import UIKit

protocol TableViewCellDelegate {
    func performSegueFor(imdbId:String)
}

class TestTableViewCell: UITableViewCell,UICollectionViewDelegate,UICollectionViewDataSource {

    var title:[String] = []
    var currentSeason:Int?
    var imdbIds:[String]?
    var tableViewCellDelegate:TableViewCellDelegate?
    override func awakeFromNib() {
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.register(UINib.init(nibName: "testCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "MemeCell")
        collectionView.backgroundColor = UIColor.clear.withAlphaComponent(0)
        
    }
    @IBOutlet weak var collectionView: UICollectionView!
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return title.count
    }
    
    fileprivate func cellDesigning(_ cell: EpisodeCollectionViewCell, _ indexPath: IndexPath) {
        
        cell.label.textColor = UIColor.white
        cell.layer.borderColor = #colorLiteral(red: 0.289356689, green: 0.01091912034, blue: 0.5677942576, alpha: 1)
        cell.layer.borderWidth = 1
        cell.layer.cornerRadius = 10
        let shadowPath2 = UIBezierPath(rect: cell.bounds)
        cell.layer.masksToBounds = false
        cell.layer.shadowColor = UIColor.black.cgColor
        cell.layer.shadowOffset = CGSize(width: CGFloat(1.0), height: CGFloat(3.0))
        cell.layer.shadowOpacity = 0.4
        cell.layer.shadowPath = shadowPath2.cgPath
        
        if indexPath.row % 2 == 0{
            cell.backgroundColor = #colorLiteral(red: 0.5, green: 0, blue: 0.8199491567, alpha: 0.5)
        }
        else {
            cell.backgroundColor = #colorLiteral(red: 0.5297341565, green: 0.08482213038, blue: 0.9868445106, alpha: 0.6)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "MemeCell", for: indexPath) as? EpisodeCollectionViewCell{
            cellDesigning(cell, indexPath)
            if indexPath.row < title.count{
                cell.label.text = title[indexPath.row]
            }
            return cell
        }
        else{
            return UICollectionViewCell()
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if let id = imdbIds?[indexPath.row]{
            tableViewCellDelegate?.performSegueFor(imdbId: id)
        }
    }
    
    
}

